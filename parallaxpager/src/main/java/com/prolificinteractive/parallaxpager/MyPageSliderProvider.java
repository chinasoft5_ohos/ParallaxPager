/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.prolificinteractive.parallaxpager;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.List;

/**
 * MyPageSliderProvider
 *
 * @since 2021-03-26
 */
public class MyPageSliderProvider extends PageSliderProvider {

    private List<BaseItem> tabComponents;

    /**
     * MyPageSliderProvider
     *
     * @param slice         上下文
     * @param tabComponents list集合
     */
    public MyPageSliderProvider(Context slice, List<BaseItem> tabComponents) {
        this.tabComponents = tabComponents;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE / 2;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component = null;
        if (componentContainer == null) {
            return component;
        }
        position = position % tabComponents.size();
        if (0 <= position && position < tabComponents.size()) {
            component = tabComponents.get(position);
        }
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object obj) {
        if (componentContainer == null) {
            return;
        }
        if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }
}