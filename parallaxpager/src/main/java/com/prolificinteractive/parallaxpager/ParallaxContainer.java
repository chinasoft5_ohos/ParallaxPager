/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxpager;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;
import com.prolificinteractive.parallaxpager.baseview.FontUtil;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * 自定义组件
 *
 * @since 2021-06-25
 */
public class ParallaxContainer extends DirectionalLayout implements PageSlider.PageChangedListener {
    private PageSlider pageSlider;
    private int componentlenth = 4;
    private ToastDialog toastDialog = null;

    public ParallaxContainer(Context context) {
        super(context);
        initview();
    }

    public ParallaxContainer(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initview();
    }

    public ParallaxContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initview();
    }

    private void initview() {
        Component ct = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_parallaxlayout, this, true);
        pageSlider = (PageSlider) ct.findComponentById(ResourceTable.Id_pageslider);
        pageSlider.addPageChangedListener(this);
    }

    /**
     * 设置适配器
     *
     * @param pageSliderProvider 适配器
     */
    public void setprovider(PageSliderProvider pageSliderProvider) {
        pageSlider.setProvider(pageSliderProvider);
    }

    @Override
    public void onPageSliding(int position, float offset, int offsetPix) {
        int pageid = position % componentlenth;
        for (int i = 0; i < componentlenth; i++) {
            if (pageid == i - 1 || pageid == i + 1) {
                BaseItem item = ParallaxViewTag.getItemList().get(pageid);
                item.onPageSlidingLast(pageid, offset, offsetPix);
            } else if (pageid == i) {
                BaseItem item = ParallaxViewTag.getItemList().get(pageid);
                item.onPageSliding(pageid, offset, offsetPix);
            }
        }
    }

    @Override
    public void onPageSlideStateChanged(int i1) {
    }

    @Override
    public void onPageChosen(int post) {
        ParallaxViewTag.setIndex(post);
        int index = post % componentlenth;
        BaseItem item = ParallaxViewTag.getItemList().get(index);
        item.setTextFont(FontUtil.getFont());
        item.onPageChosen(index);
        toastDialog.showToast();
    }

    /**
     * 定义外部接口
     *
     * @since 2021-06-25
     */
    public interface ToastDialog {
        /**
         * 显示tost
         */
        void showToast();
    }

    public void setToastDialog(ToastDialog toastlog) {
        this.toastDialog = toastlog;
    }

    /**
     * 设置当前页面的个数
     *
     * @param length 长度
     * @return 返回到当前页面
     */
    public int setlength(int length) {
        componentlenth = length;
        return componentlenth;
    }
}
