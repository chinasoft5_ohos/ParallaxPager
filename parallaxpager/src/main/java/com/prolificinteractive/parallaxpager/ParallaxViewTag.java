/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxpager;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;

import java.util.ArrayList;
import java.util.List;

/**
 * 实体类
 *
 * @since 2021-06-25
 */
public class ParallaxViewTag {
    private static int index;
    private static List<BaseItem> itemList;

    private ParallaxViewTag() {

    }

    public static int getIndex() {
        return index;
    }

    public static void setIndex(int index) {
        ParallaxViewTag.index = index;
    }

    public static List<BaseItem> getItemList() {
        return itemList;
    }

    /**
     * 设置子控件的集合
     *
     * @param itemList 集合
     */
    public static void setItemList(List<BaseItem> itemList) {
        ParallaxViewTag.itemList = new ArrayList<>();
        for (BaseItem baseItem : itemList) {
            ParallaxViewTag.itemList.add(baseItem);
        }
    }
}
