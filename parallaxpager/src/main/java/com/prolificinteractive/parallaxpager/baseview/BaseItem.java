/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.prolificinteractive.parallaxpager.baseview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

public abstract class BaseItem extends DirectionalLayout implements PageScrollListener {

    public BaseItem(Context context) {
        this(context, null);
    }

    public BaseItem(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public BaseItem(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        LayoutConfig config = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        setLayoutConfig(config);
        Component root = LayoutScatter.getInstance(context).parse(getLayoutRes(), this, true);
        initView(root);

    }

    /**
     * 当前页面
     *
     * @param var1      当前标识
     * @param offset    偏移量
     * @param offsetPix xy轴偏移量
     */
    public void onPageSliding(int var1, float offset, int offsetPix) {

        if (offsetPix > 0) {
            scrollToLeft(var1, offset, offsetPix);
        } else {
            scrollToRight(var1, offset, offsetPix);
        }
    }

    public void onPageSlidingLast(int var1, float offset, int offsetPix) {

        if (offsetPix > 0) {
            LastToLeft(var1, offset, offsetPix);
        } else {
            LastToRight(var1, offset, offsetPix);
        }
    }

    protected abstract void scrollToRight(int var1, float offset, float offsetPix);

    protected abstract void scrollToLeft(int var1, float offset, float offsetPix);

    protected abstract void LastToRight(int var1, float offset, float offsetPix);

    protected abstract void LastToLeft(int var1, float offset, float offsetPix);

    protected abstract void initView(Component root);

    protected abstract int getLayoutRes();

}
