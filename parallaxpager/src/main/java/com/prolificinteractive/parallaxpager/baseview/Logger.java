package com.prolificinteractive.parallaxpager.baseview;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {
    private static final String TAG = "VideoCache";
    public static boolean IS_DEBUG = false;
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x01, TAG);

    public static void debug(String msg) {
        if (IS_DEBUG) {
            HiLog.debug(label, msg);
        }
    }

    public static void info(String msg) {
        if (IS_DEBUG) {
            HiLog.info(label, msg);
        }
    }

    public static void warn(String msg) {
        if (IS_DEBUG) {
            HiLog.warn(label, msg);
        }
    }

    public static void error(String msg) {
        if (IS_DEBUG) {
            HiLog.error(label, msg);
        }
    }
}
