/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxpager.baseview;

import ohos.agp.text.Font;

/**
 * Full PageScrollListener ability
 *
 * @since 2021-06-27
 */
public interface PageScrollListener {
    /**
     * 输出移动距离
     *
     * @param var1 当前页面
     * @param var2 平移位置
     * @param var3 透明度
     */
    void onPageSliding(int var1, float var2, int var3);

    /**
     * 翻转中
     *
     * @param i2 1
     */
    void onPageChosen(int i2);

    /**
     * 设置文本样式
     *
     * @param font 样式
     */
    void setTextFont(Font font);

}
