/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxpager.baseview;

import ohos.agp.text.Font;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;

import java.io.*;

/**
 * 解析ttf文件工具
 *
 * @since 2021-04-22
 */
public class FontUtil {
    private static Font font;
    private static OutputStream outputStream = null;
    private static Resource resource = null;

    public static Font getFont() {
        return font;
    }

    public static void setFont(Font font) {
        FontUtil.font = font;
    }

    /**
     * 使用流解析解析ttf文件
     *
     * @param context 上下文
     * @param name    ttf文件名字
     * @return 解析后的Font
     */
    public static Font createFontBuild(Context context, String name) {

        ResourceManager resManager = context.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + name);
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            Logger.debug("this is ioe");
        }
        StringBuffer fileName = new StringBuffer(name);
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName.toString());
        try {

            outputStream = new FileOutputStream(file);
            byte[] bytes = new byte[1024];
            int index;
            while ((index = resource.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (FileNotFoundException e) {
            Logger.debug("this is file");
        } catch (IOException e) {
            Logger.debug("this is ioetion");
        } finally {
            try {
                resource.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        Font.Builder builder = new Font.Builder(file);
        return builder.build();
    }

    /**
     * 根据emoji序号返回emoji表情字符串
     *
     * @param id id
     * @return emoji表情字符串
     */
    public static String convertEmoji(String id) {
        String returnedEmoji = "";
        try {
            int convertEmojiToInt = Integer.parseInt(id.substring(2), 16);
            returnedEmoji = getEmojiByUnicode(convertEmojiToInt);
        } catch (NumberFormatException e) {
            returnedEmoji = "";
        }
        return returnedEmoji;
    }

    private static String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }
}
