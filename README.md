# ParallaxPager

#### 项目介绍
- 项目名称：ParallaxPager  
- 所属系列：openharmony第三方组件适配移植  
- 功能：使用视差效果为你的Ohos滚动添加一些深度  
- 项目移植状态：主功能完成  
- 调用差异：有  
- 开发版本：sdk6，DevEco Studio2.2 Beta1  
- 基线版本: Release 3.0.0

#### 效果演示
![example](bg_parallax_pager.gif)

#### 安装教程
1.在moudle级别下的build.gradle文件中添加依赖，在dependencies标签中增加对libs目录下jar包的引用  

 ```gradle
 // 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}
// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:ParallaxPager:1.0.0'   
}
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
* xml属性
```
<com.prolificinteractive.parallaxpager.ParallaxContainer
            ohos:id="$+id:parallax_container"
            ohos:height="match_parent"        
            ohos:width="match_parent"/>

```
* Java 调用示例 
 提供一个父类(BaseItem)开发者如有多个界面可继承此类以及,参照demo进行实现页面功能!
```
 @Override
 protected void scrollToRight(int var1, float offset, float offsetPix) {

 }
  @Override
  protected void scrollToLeft(int var1, float offset, float offsetPix) {

}
 @Override
 protected void LastToRight(int var1, float offset, float offsetPix) {
}
 @Override
 protected void LastToLeft(int var1, float offset, float offsetPix) {
}
以上四个回调方法对当前页面或相邻页面进行动画的设置和透明度的处理
```
* 用list集合去存储需要展示在pageslide中的页面
```
 private List<BaseItem> initItems() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new ComponentOne(this));
        items.add(new ComponentTwo(this));
        items.add(new ComponentThere(this));
        items.add(new ComponentFour(this));
        ParallaxViewTag.setItemList(items);
        return items;
    }
```
* 自定义组件的获取和使用
```
        List<BaseItem> itemList = initItems();
         parallax = (ParallaxContainer) findComponentById(ResourceTable.Id_parallax);
         parallax.setlength(itemList.size());
         MyPageSliderProvider provider = new MyPageSliderProvider(this, itemList);
         parallax.setprovider(provider);
```

* 滑动时ToastDialog显示的使用,toast对象可按自己的需求随意定义

```
 ParallaxContainer.ToastDialog toastDialog = new ParallaxContainer.ToastDialog() {
             @Override
             public void ShowToast() {
                 toast.text("Page Selected: " + ParallaxViewTag.getIndex()).show();
             }
         };
         parallax.setToastDialog(toastDialog);
```
#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与ohos原组件基本无差异  

#### 版本迭代

- 1.0.0

#### 版权和许可信息  
```
Copyright (c) 2018 Prolific Interactive.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```

