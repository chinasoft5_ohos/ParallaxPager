/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.component;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;
import com.prolificinteractive.parallaxproject.ResourceTable;
import com.prolificinteractive.parallaxproject.component.showuntil.Fouruntil;
import com.prolificinteractive.parallaxproject.component.showuntil.NumberUntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Oneuntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Thereuntil;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.app.Context;

/**
 * Full component ability
 *
 * @since 2021-06-25
 */
public class ComponentFour extends BaseItem {
    private Image image1;
    private Image image2;
    private Text text1;
    private Text text2;

    /**
     * ComponentFour
     *
     * @param context q
     */
    public ComponentFour(Context context) {
        super(context);
    }

    @Override
    protected void initView(Component root) {
        image1 = (Image) root.findComponentById(ResourceTable.Id_Four_img1);
        image2 = (Image) root.findComponentById(ResourceTable.Id_Four_img2);
        text1 = (Text) root.findComponentById(ResourceTable.Id_Four_text1);
        text2 = (Text) root.findComponentById(ResourceTable.Id_Four_text2);
        Fouruntil.setImage1(image1);
        Fouruntil.setImage2(image2);
        Fouruntil.setText1(text1);
        Fouruntil.setText2(text2);
    }

    @Override
    protected int getLayoutRes() {
        return ResourceTable.Layout_component_four;
    }

    @Override
    protected void scrollToRight(int var1, float offset, float offsetPix) {
        float off = 1.0f + offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth();
        image1.setAlpha(off);
        image1.setTranslationX(0 - offsetPix * NumberUntil.getAnInt2());

        image2.setTranslationX(0 - offsetPix * NumberUntil.getAnInt2());

        text1.setTranslationX(offsetPix * NumberUntil.getAnInt3());
        text1.setTranslationY(offsetPix * NumberUntil.getAnInt4());

        text2.setTranslationX(0 - offsetPix * NumberUntil.getAnInt2());
        text2.setTranslationY(0 - offsetPix * NumberUntil.getAnInt3());
    }

    @Override
    protected void scrollToLeft(int var1, float offset, float offsetPix) {
        float off = 1.0f - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth();

        image1.setTranslationX(-offsetPix * NumberUntil.getAnInt3());

        image2.setAlpha(off);
        image2.setTranslationX(-offsetPix * NumberUntil.getAnInt3());

        text1.setAlpha(off);
        text1.setTranslationY(-offsetPix * NumberUntil.getAnInt3());
        text1.setTranslationX(-offsetPix * NumberUntil.getAnInt4());

        text2.setAlpha(off);
        text2.setTranslationX(offsetPix * NumberUntil.getAnInt2());
        text2.setTranslationY(offsetPix * NumberUntil.getAnInt3());
    }

    @Override
    protected void LastToRight(int var1, float offset, float offsetPix) {
        float off = 0 - (offsetPix / getEstimatedWidth());
        Thereuntil.getImage1().setAlpha(off);
        Thereuntil.getImage1().setTranslationX(-((getEstimatedWidth() + offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20()));
        Thereuntil.getImage2().setAlpha(off);
        Thereuntil.getImage2().setTranslationX(-((getEstimatedWidth() + offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20()));
        Thereuntil.getText1().setTranslationX(-(getEstimatedWidth() + offsetPix + 1));
        Thereuntil.getText2().setTranslationX(-(getEstimatedWidth() + offsetPix + 1));
    }

    @Override
    protected void LastToLeft(int var1, float offset, float offsetPix) {
        float off = offsetPix / getEstimatedWidth();
        Oneuntil.getImage1().setAlpha(off);
        Oneuntil.getImage2().setAlpha(off);
        Oneuntil.getImage1().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt10()) / NumberUntil.getAnInt10());
        Oneuntil.getText1().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt2()) / NumberUntil.getAnInt2());

        Oneuntil.getImage2().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt10()) / NumberUntil.getAnInt10());
        Oneuntil.getText2().setTranslationX(-((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt2()) / NumberUntil.getAnInt2()));
    }

    @Override
    public void onPageChosen(int i) {
        text1.setAlpha(1f);
        text1.setTranslationX(0f);
        text1.setTranslationY(0f);

        text2.setAlpha(1f);
        text2.setTranslationX(0f);
        text2.setTranslationY(0f);

        image1.setAlpha(1f);
        image1.setTranslationX(0f);
        image1.setTranslationY(0f);

        image2.setAlpha(1f);
        image2.setTranslationX(0f);
        image2.setTranslationY(0f);
    }

    @Override
    public void setTextFont(Font font) {
        text1.setFont(font);
        text2.setFont(font);
    }
}
