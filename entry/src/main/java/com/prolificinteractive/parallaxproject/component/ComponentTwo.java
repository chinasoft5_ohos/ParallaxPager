/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.component;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;
import com.prolificinteractive.parallaxproject.ResourceTable;
import com.prolificinteractive.parallaxproject.component.showuntil.NumberUntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Oneuntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Thereuntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Twountil;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.app.Context;

/**
 * Full componenttwo ability
 *
 * @since 2021-06-25
 */
public class ComponentTwo extends BaseItem {
    private Image image1;
    private Image image2;
    private DirectionalLayout twoide;
    private Text text2;
    private Text twoearch;
    private Text twohome;

    /**
     * 构造方法1
     *
     * @param context q
     */
    public ComponentTwo(Context context) {
        super(context);
    }

    @Override
    protected void initView(Component root) {
        image1 = (Image) root.findComponentById(ResourceTable.Id_two_img1);
        image2 = (Image) root.findComponentById(ResourceTable.Id_two_img2);
        text2 = (Text) root.findComponentById(ResourceTable.Id_two_text2);
        twoide = (DirectionalLayout) root.findComponentById(ResourceTable.Id_two_ide);
        twoearch = (Text) root.findComponentById(ResourceTable.Id_two_earch);
        twohome = (Text) root.findComponentById(ResourceTable.Id_two_home);
        Twountil.setImage1(image1);
        Twountil.setImage2(image2);
        Twountil.setText2(text2);
        Twountil.setTwoearch(twoearch);
        Twountil.setTwohome(twohome);
        Twountil.setTwoide(twoide);
    }

    @Override
    protected int getLayoutRes() {
        return ResourceTable.Layout_component_two;
    }

    @Override
    protected void scrollToRight(int var1, float offset, float offsetPix) {
        float off = (float) (1.0f + offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());

        image1.setAlpha(off);
        image1.setTranslationY(offsetPix);
        image1.setTranslationX(0 - offsetPix);

        image2.setAlpha(off);
        image2.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3());

        twoide.setAlpha(off);
        twoide.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3());

        text2.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3());
    }

    @Override
    protected void scrollToLeft(int var1, float offset, float offsetPix) {
        float off = (float) (1.0f - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());

        image1.setAlpha(off);
        image2.setAlpha(off);
        image2.setTranslationY(offsetPix * NumberUntil.getAnInt3());
        twoide.setAlpha(off);
        twoide.setTranslationX(-offsetPix * NumberUntil.getAnInt3());

        text2.setTranslationX(offsetPix * NumberUntil.getAnInt3());
    }

    @Override
    protected void LastToRight(int var1, float offset, float offsetPix) {
        float off = 0 - (float) (offsetPix / getEstimatedWidth());
        Oneuntil.getImage1().setAlpha(off);
        Oneuntil.getImage1().setTranslationX(-(getEstimatedWidth() + offsetPix
                - NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Oneuntil.getImage2().setAlpha(off);
        Oneuntil.getImage2().setTranslationX(-(getEstimatedWidth() + offsetPix
                - NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());

        Oneuntil.getText1().setTranslationX(-(getEstimatedWidth() + offsetPix - 1));
        Oneuntil.getText2().setTranslationX((getEstimatedWidth() + offsetPix
                - NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Oneuntil.getText2().setTranslationY(getEstimatedWidth() + offsetPix - 1);
    }

    @Override
    protected void LastToLeft(int var1, float offset, float offsetPix) {
        float off = (float) (offsetPix / getEstimatedWidth());
        Thereuntil.getImage1().setAlpha(off);
        Thereuntil.getImage1().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt5()) / NumberUntil.getAnInt5());

        Thereuntil.getImage2().setAlpha(off);
        Thereuntil.getImage2().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Thereuntil.getImage2().setTranslationY((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt2()) / NumberUntil.getAnInt2());

        Thereuntil.getText1().setTranslationX(getEstimatedWidth() - offsetPix + 1);
        Thereuntil.getText2().setTranslationX(getEstimatedWidth() - offsetPix + 1);
    }

    @Override
    public void onPageChosen(int i) {
        twoide.setAlpha(1f);
        twoide.setTranslationX(0f);
        twoide.setTranslationY(0f);

        text2.setAlpha(1f);
        text2.setTranslationX(0f);
        text2.setTranslationY(0f);

        image1.setAlpha(1f);
        image1.setTranslationX(0f);
        image1.setTranslationY(0f);

        image2.setAlpha(1f);
        image2.setTranslationX(0f);
        image2.setTranslationY(0f);
    }

    @Override
    public void setTextFont(Font font) {
        twoearch.setFont(font);
        twohome.setFont(font);
        text2.setFont(font);
    }
}
