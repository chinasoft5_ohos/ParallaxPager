/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.component;

import com.prolificinteractive.parallaxpager.baseview.BaseItem;
import com.prolificinteractive.parallaxpager.baseview.FontUtil;
import com.prolificinteractive.parallaxproject.ResourceTable;
import com.prolificinteractive.parallaxproject.component.showuntil.Fouruntil;
import com.prolificinteractive.parallaxproject.component.showuntil.NumberUntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Oneuntil;
import com.prolificinteractive.parallaxproject.component.showuntil.Twountil;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.app.Context;

/**
 * Full component1 ability
 *
 * @since 2021-06-25
 */
public class ComponentOne extends BaseItem {
    private Image image1;
    private Image image2;
    private Text text1;
    private Text text2;

    /**
     * ComponentOne构造方法
     *
     * @param context 上下文
     */
    public ComponentOne(Context context) {
        super(context);
    }

    @Override
    protected void initView(Component root) {
        image1 = (Image) root.findComponentById(ResourceTable.Id_image1);
        image2 = (Image) root.findComponentById(ResourceTable.Id_image2);
        text1 = (Text) root.findComponentById(ResourceTable.Id_text1);
        text2 = (Text) root.findComponentById(ResourceTable.Id_text2);
        text1.setFont(FontUtil.getFont());
        text2.setFont(FontUtil.getFont());

        Oneuntil.setImage1(image1);
        Oneuntil.setImage2(image2);
        Oneuntil.setText1(text1);
        Oneuntil.setText2(text2);
    }

    @Override
    protected int getLayoutRes() {
        return ResourceTable.Layout_component_one;
    }

    @Override
    protected void scrollToRight(int var1, float offset, float offsetPix) {
        float off = (float) (1.0f + offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());
        image1.setAlpha(off);
        image1.setTranslationY(0 - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());
        image1.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());

        image2.setAlpha(off);
        image2.setTranslationY(0 - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());
        image2.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3() / getEstimatedWidth());

        text1.setTranslationX(0 - offsetPix * NumberUntil.getAnInt3());

        text2.setTranslationX(offsetPix * NumberUntil.getAnInt5());
    }

    @Override
    protected void scrollToLeft(int var1, float offset, float offsetPix) {
        float off = (float) (1.0f - offsetPix * NumberUntil.getAnInt4() / getEstimatedWidth());
        image1.setAlpha(off);
        image2.setAlpha(off);

        text1.setTranslationX(-offsetPix * NumberUntil.getAnInt3());

        text2.setTranslationY(offsetPix * NumberUntil.getAnInt2());
        text2.setTranslationX(offsetPix * NumberUntil.getAnInt5());
    }

    @Override
    protected void LastToRight(int var1, float offset, float offsetPix) {
        float off = 0 - (float) (offsetPix / getEstimatedWidth());
        Fouruntil.getImage1().setAlpha(off);
        Fouruntil.getImage2().setAlpha(off);
        Fouruntil.getText1().setTranslationX(-(getEstimatedWidth() + offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Fouruntil.getText1().setTranslationY(-(getEstimatedWidth() + offsetPix + 1));
        Fouruntil.getText2().setTranslationX(-(getEstimatedWidth() + offsetPix
                + NumberUntil.getAnInt10()) / NumberUntil.getAnInt10());
        Fouruntil.getText2().setTranslationY(-(getEstimatedWidth() + offsetPix + 1));
    }

    @Override
    protected void LastToLeft(int var1, float offset, float offsetPix) {
        float off = (float) (offsetPix / getEstimatedWidth());
        Twountil.getImage1().setAlpha(off);
        Twountil.getImage1().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Twountil.getImage1().setTranslationY(-(getEstimatedWidth() - offsetPix + 1));

        Twountil.getImage2().setTranslationX((getEstimatedWidth() - offsetPix
                + NumberUntil.getAnInt20()) / NumberUntil.getAnInt20());
        Twountil.getImage1().setAlpha(off);

        Twountil.getText2().setTranslationX(getEstimatedWidth() - offsetPix + 1);
        Twountil.getTwoide().setTranslationX(getEstimatedWidth() - offsetPix + 1);
    }

    @Override
    public void onPageChosen(int i2) {
        text1.setTranslationX(0f);
        text1.setTranslationY(0f);

        text2.setTranslationX(0f);
        text2.setTranslationY(0f);

        image1.setAlpha(1f);
        image1.setTranslationX(0f);
        image1.setTranslationY(0f);

        image2.setAlpha(1f);
        image2.setTranslationX(0f);
        image2.setTranslationY(0f);
    }

    @Override
    public void setTextFont(Font font) {
        text1.setFont(font);
        text2.setFont(font);
    }
}
