/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.component.showuntil;

import ohos.agp.components.Image;
import ohos.agp.components.Text;

/**
 * Full Fouruntil ability
 *
 * @since 2021-06-25
 */
public class Fouruntil {
    private static Image image1;
    private static Image image2;
    private static Text text1;
    private static Text text2;

    private Fouruntil() {
    }

    public static Image getImage1() {
        return image1;
    }

    public static void setImage1(Image image1) {
        Fouruntil.image1 = image1;
    }

    public static Image getImage2() {
        return image2;
    }

    public static void setImage2(Image image2) {
        Fouruntil.image2 = image2;
    }

    public static Text getText1() {
        return text1;
    }

    public static void setText1(Text text1) {
        Fouruntil.text1 = text1;
    }

    public static Text getText2() {
        return text2;
    }

    public static void setText2(Text text2) {
        Fouruntil.text2 = text2;
    }
}
