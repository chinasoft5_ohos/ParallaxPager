/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.component.showuntil;

import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

/**
 * Full Twountil ability
 *
 * @since 2021-06-25
 */
public class Twountil {
    private static Image image1;
    private static Image image2;
    private static Text twoearch;
    private static Text text2;
    private static Text twohome;
    private static DirectionalLayout twoide;

    private Twountil() {
    }

    public static Image getImage1() {
        return image1;
    }

    public static void setImage1(Image image1) {
        Twountil.image1 = image1;
    }

    public static Image getImage2() {
        return image2;
    }

    public static void setImage2(Image image2) {
        Twountil.image2 = image2;
    }

    public static Text getTwoearch() {
        return twoearch;
    }

    public static void setTwoearch(Text twoearch) {
        Twountil.twoearch = twoearch;
    }

    public static Text getText2() {
        return text2;
    }

    public static void setText2(Text text2) {
        Twountil.text2 = text2;
    }

    public static Text getTwohome() {
        return twohome;
    }

    public static void setTwohome(Text twohome) {
        Twountil.twohome = twohome;
    }

    public static DirectionalLayout getTwoide() {
        return twoide;
    }

    public static void setTwoide(DirectionalLayout twoide) {
        Twountil.twoide = twoide;
    }
}
