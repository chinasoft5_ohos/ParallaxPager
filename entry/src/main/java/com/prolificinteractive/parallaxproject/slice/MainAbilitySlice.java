/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.prolificinteractive.parallaxproject.slice;

import com.muddzdev.styleabletoast.styleabletoast.StyleableToast;
import com.prolificinteractive.parallaxpager.MyPageSliderProvider;
import com.prolificinteractive.parallaxpager.ParallaxContainer;
import com.prolificinteractive.parallaxpager.ParallaxViewTag;
import com.prolificinteractive.parallaxpager.baseview.BaseItem;
import com.prolificinteractive.parallaxpager.baseview.FontUtil;
import com.prolificinteractive.parallaxproject.NewAbility;
import com.prolificinteractive.parallaxproject.ResourceTable;
import com.prolificinteractive.parallaxproject.component.ComponentFour;
import com.prolificinteractive.parallaxproject.component.ComponentOne;
import com.prolificinteractive.parallaxproject.component.ComponentThere;
import com.prolificinteractive.parallaxproject.component.ComponentTwo;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Full screen ability slice
 *
 * @since 2021-06-25
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private Font font;
    private Button butid;
    private Text textplan;
    private StyleableToast.Builder toast;
    private ParallaxContainer parallax;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_layout_main);
        toast = new StyleableToast.Builder(getContext())
                .font("Bitter-Bold.ttf")
                .textColor(Color.BLACK)
                .backgroundColor(Color.WHITE);
        font = FontUtil.createFontBuild(this, "Bitter-Bold.ttf");
        FontUtil.setFont(font);

        List<BaseItem> itemList = initItems();
        parallax = (ParallaxContainer) findComponentById(ResourceTable.Id_parallax);
        parallax.setlength(itemList.size());
        MyPageSliderProvider provider = new MyPageSliderProvider(this, itemList);
        parallax.setprovider(provider);
        ParallaxContainer.ToastDialog toastDialog = new ParallaxContainer.ToastDialog() {
            @Override
            public void showToast() {
                toast.text("Page Selected: " + ParallaxViewTag.getIndex()).show();
            }
        };
        parallax.setToastDialog(toastDialog);
        textplan = (Text) findComponentById(ResourceTable.Id_textplan);
        textplan.setFont(font);
        butid = (Button) findComponentById(ResourceTable.Id_butid);
        butid.setFont(font);
        butid.setClickedListener(this::onClick);
    }

    private List<BaseItem> initItems() {
        List<BaseItem> items = new ArrayList<>();
        items.add(new ComponentOne(this));
        items.add(new ComponentTwo(this));
        items.add(new ComponentThere(this));
        items.add(new ComponentFour(this));
        ParallaxViewTag.setItemList(items);
        return items;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        if (ParallaxViewTag.getIndex() != 0) {
            toast.text("Page Selected: " + ParallaxViewTag.getIndex()).show();
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_butid:
                Intent secondIntent = new Intent(); // 指定待启动FA的bundleName和abilityName
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(this.getBundleName())
                        .withAbilityName(NewAbility.class.getName())
                        .build();
                secondIntent.setOperation(operation); // 通过AbilitySlice的startAbility接口实现启动另一个页面
                startAbility(secondIntent);
                break;
            default:
                break;
        }
    }
}
